//
// Created by saevar on 10/31/17.
//

#define PACK

#ifndef SRC_COMMON_H
#define SRC_COMMON_H

#define INPUT_MEM               0x00000000
#define OUTPUT_MEM              0x80000000

#define READY 1
#define BUSY -1
#define IDLE 0

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
}PACK pixel;

typedef struct {
    int stream_number;
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    volatile int rect_height;
    unsigned char data[];
} PACK transfer;

#endif //SRC_COMMON_H