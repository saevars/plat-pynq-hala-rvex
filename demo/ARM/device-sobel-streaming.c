//
// Created by saevar on 2/16/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "palloc.h"
#include "hala_rvex.h"


int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("missing input image_data to process \n");
        return 1;
    }

    int error = 0;
    int number_of_cores = 7;
    rvex device;
    context context;
    const char *input_file = argv[1];
    int streams[] = {1, 2, 4, 5, 8, 10};

    // Load input image_data and dimensions to memory
    long size;
    unsigned char *image_data;
    int width;
    int height;
    int pixel_size;

    printf("reading image dimensions\n");
    error = get_image_dimensions(input_file, &width, &height, &pixel_size);
    if (error) {
        printf("failed to get image dimensions \n");
        return 1;
    }
    size = width * height * pixel_size;
    // Allocate memory for image width padding at top and bottom
    image_data = calloc((size_t) size, sizeof(unsigned char));

    printf("reading image file\n");
    error = get_image_from_file(input_file, image_data, size);
    if (error) {
        printf("failed to read data from image file \n");
        return (1);
    }
    printf("setting image properties\n");
    image_info input_image, output_image;

    // Get contiguous memory buffers
    int fb_address;
    error = get_memory_address(size, &fb_address);
    if (error) {
        printf("failed to allocate memory \n");
        return 1;
    }
    printf("initialising context\n");
    error = initialise_context(&context);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }
    int start_seconds = (int) time(NULL);
    clock_t start = clock();
    for (int i = 0; i < 1; ++i) {
        int number_of_streams = streams[i];
        printf("\n\n\nnumber of streams %d \n", streams[i]);
        error = set_image_properties(&input_image, image_data, width, height, pixel_size, number_of_streams, 0, 0, 0, 0);
        if (error) {
            printf("failed to set image properties \n");
            return (1);
        }
        input_image.number_of_rects = 20;
        input_image.out_rect.width = 800;
        input_image.out_rect.height = 35;
        input_image.out_rect.padded_width = 1601;

        output_image = input_image;
        printf("output image pixel size %d \n", output_image.pixel_size);

//        error = greyscale_image(&output_image, input_image);
//        if (error) {
//            printf("failed to set greyscale image \n");
//            return (1);
//        }


        /* Load bitstream to memory as *bitstream
         * Download the right bitstream for the number of cores and streams
         */
        char *bitstream = NULL;
        size_t bitstream_size;


        printf("Reading bitstream from file \n");
        error = read_bitstream_from_file(&context,
                                         number_of_streams,
                                         number_of_cores,
                                         &bitstream,
                                         &bitstream_size);
        if (error) {
            perror("Failed to read bitstream from file\n");
            return 1;
        }


        //TODO abstract bitsteram part as part of device
        // Download the bitstream to the fabric
        printf("Downloading bitstream to fabric \n");
        error = download_bitstream(bitstream, bitstream_size);
        if (error) {
            perror("Failed to download bitstream to PL\n");
            return 1;
        }
        free(bitstream);

        printf("initialising rvex \n");
        // initialise rVEXes and memory-map cores
//    rvex device;
        error = initialise_rvex(&context, &device, number_of_streams, number_of_cores);
        if (error) {
            printf("failed to initialise rvex \n");
            return 1;
        }

        printf("downloading kernels \n");
        // Download kernels to rVEX cores
        error = create_program_with_binary(&context, &device, "streaming-sobel");
        if (error) {
            perror("failed to download kernel \n");
            return 1;
        }

        printf("setting up parmeters\n");
        // Setup default transfer parameters
        error = set_rvex_parameters(&device, fb_address, input_image, output_image);
        if (error) {
            perror("failed to set rVEX parameters \n");
            return 1;
        }

        printf("starting up rVEX cores\n");
        start_rvex_programs(device);

        int iterations = 2000;
        printf("writing image to rvex \n");
        printf("number of iterations: %d \n", iterations);
        int start_frame_seconds = (int) time(NULL);
        for (int j = 0; j < iterations; ++j) {
            error = write_padded_lines_to_rvex(&device, input_image);
            if (error) {
                perror("failed to write image ro rVEX \n");
                return 1;
            }
        }
        int stop_frame_seconds = (int) time(NULL);
        double delta = stop_frame_seconds - start_frame_seconds;
        double fps = iterations / delta;
        printf("time per image %f \n", delta / iterations);
        printf("fps: %f \n", fps);

    }
    clock_t stop = clock();
    double init = (stop - start) / (double) CLOCKS_PER_SEC;
    printf("\n\n\n\nInit: %f\n", init / 4);
    int stop_seconds = (int) time(NULL);
    double delta = stop_seconds - start_seconds;
    printf("time to reconfigure %f \n", delta / 4);

    printf("retrieving image from memory\n");
    write_image_to_file(context, fb_address, output_image.size, input_file);

    // Send free request to memory server
    error = free_buffers();
    if (!error) {
        printf("buffers freed\n");
    }
    free_context(&context);
//    free(bitstream);
    free(image_data);
    return 0;
}




