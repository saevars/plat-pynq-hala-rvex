//
// Created by saevar
// on 11/27/17.
//
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>
#include "hala_rvex.h"
#include "common.h"


void *write_fixed_lines(void *arg);

/*
 * Functions that help initialise the platform
 * and communicate with the hala-rvex
 * Reads platform specific parameters from config file
 *
 * */

int initialise_context(context *ctx) {
    char val[1024];
    char param[1024];
    FILE *file = fopen("conf/config.conf", "r");
    if (file == NULL) {
        printf("could not open configuration file: conf/config.conf\n");
        return 1;
    }
    char line[1024];
    while (fgets(line, sizeof(line), file) != NULL) {
        sscanf(line, "%s %s\n", param, val);
        if (strcmp(param, "NUMBER_OF_STREAMS") == 0) {
            ctx->number_of_streams = atoi(val);
        } else if (strcmp(param, "NUMBER_OF_CORES") == 0) {
            ctx->number_of_cores = atoi(val);
        } else if (strcmp(param, "CORE_OFFSET") == 0) {
            ctx->core_offset = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "STREAM_OFFSET") == 0) {
            ctx->stream_offset = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "RVEX_BASE_ADDRESS") == 0) {
            ctx->rvex_base_address = strtoul(val, NULL, 16);
        } else if (strcmp(param, "DMEM_SIZE") == 0) {
            ctx->dmem_size = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "IMEM_SIZE") == 0) {
            ctx->imem_size = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "DMEM_OFFSET") == 0) {
            ctx->dmem_offset = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "IMEM_OFFSET") == 0) {
            ctx->imem_offset = (int) strtol(val, NULL, 16);
        } else if (strcmp(param, "CREG_OFFSET") == 0) {
            ctx->creg_offset = (int) strtol(val, NULL, 16);
        }
    }
    fclose(file);
    ctx->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}

int download_bitstream(char *buffer, size_t size) {

    // Open device configuration to download bitstream
    FILE *xdev = fopen("/dev/xdevcfg", "wb");
    if (xdev == NULL) {
        printf("Failed to open xdevcfg \n");
        return 1;
    }
    fwrite(buffer, 1, size, xdev);
    fclose(xdev);
    return 0;
}

/*
 * Function that reads bitstream from file into memory
 * Naming convention for bitstream file names is 'rvex_#streams_#cores.bit
 * and have to be in a ../overlay folder relative to the program run directory
 *
 * @param number_of_streams is the total number of hala_rvex streams on the board
 *
 * @param number_of_cores is the number of cores per stream, i.e. the depth of the
 * hala_rvex pipeline
 *
 * @param buffer does not need to be allocated beforehand as the function thakes
 * care of that. Needs to be freed by the caller
 */
int read_bitstream_from_file(context *ctx, int number_of_streams, int number_of_cores, char **buffer, size_t *size) {
    if (number_of_cores * number_of_streams > ctx->number_of_cores) {
        printf("Maximum number of rvex processors is %d \n", ctx->number_of_cores);
    }
    char bit_stream_name[32];
    //Naming convention for overlays is rvex_streams_cores.bit
    snprintf(bit_stream_name, sizeof(bit_stream_name), "../overlays/rvex_%d_%d.bit", number_of_streams,
             number_of_cores);

    FILE *bit_stream = fopen(bit_stream_name, "rb");
    if (bit_stream == NULL) {
        printf("Could not open bit stream file: %s\n", bit_stream_name);
        return 1;
    }

    fseek(bit_stream, 0, SEEK_END);
    *size = (size_t) ftell(bit_stream);
    rewind(bit_stream);

    *buffer = malloc(sizeof(char) * *size);
    fread(*buffer, 1, *size, bit_stream);
    fclose(bit_stream);

    return 0;
}

// Byteswapping functions for talking to the r-VEX.
int byte_swap_w(int word) {
    return ((word & 0xff) << 24) | ((word & 0xff00) << 8) | ((word >> 8) & 0xff00) | ((word >> 24) & 0xff);
}

/*
 * Function that initialises all streams and cores of every stream
 * with memory addresses for data and instruction memory.
 * This function also offsets the input frame buffer and gives
 * every stream a number
 */
int initialise_rvex(context *context, rvex *rvex, int number_of_streams, int number_of_cores) {
    if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }
    if (number_of_cores * number_of_streams > context->number_of_cores) {
        printf("Maximum number of rvex processors is %d \n", context->number_of_cores);
    }
    long base_address = context->rvex_base_address;
    rvex->base_address = base_address;
    rvex->number_of_streams = number_of_streams;
    rvex->number_of_cores = number_of_cores;
    // Get memory for the number of streams
    rvex->streams = (rvex_stream *) malloc(sizeof(rvex_stream) * number_of_streams);
    // Loop through the streams and assign input and data memories
    for (int stream = 0; stream < number_of_streams; stream++) {
        // loop though cores and map instruction memories and CREGs
        rvex->streams[stream].cores = malloc(sizeof(rvex_core) * number_of_cores);
        for (int core = 0; core < number_of_cores; ++core) {
            // memory offset for every instruction memory of every core
            long imem_address =
                    base_address + context->imem_offset + core * context->core_offset + stream * context->stream_offset;
            off_t creg_address =
                    base_address + context->creg_offset + core * context->core_offset + stream * context->stream_offset;
            rvex->streams[stream].cores[core].imem_address = imem_address;
            rvex->streams[stream].cores[core].creg_address = creg_address;
            rvex->streams[stream].cores[core].creg = (int *) mmap(
                    NULL,
                    (size_t) context->imem_size,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    creg_address);
            rvex->streams[stream].cores[core].imem = (char *) mmap(
                    NULL,
                    (size_t) context->imem_size,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    imem_address);
        }
        // Find offset for the data memory of first core of every stream
        off_t dmem_address = base_address + context->dmem_offset + stream * context->stream_offset;
        rvex->streams[stream].base = mmap(
                NULL,
                (size_t) context->dmem_size,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                context->memory_handler,
                dmem_address);

        if (context->memory_handler == (int) MAP_FAILED) {
            perror("rvex mapping for absolute memory access failed.\n");
            return 1;
        }
        rvex->streams[stream].number_of_streams = number_of_streams;
        rvex->streams[stream].stream_number = stream;
        rvex->streams[stream].parameters = rvex->streams[stream].base;
    }
    return 0;
}


int download_binary_to_rvex(rvex_core *core, char program[], int rvex_imem_size) {
    FILE *program_file = fopen(program, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", program);
        return 1;
    }
    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);

    if (lSize > rvex_imem_size) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", program, rvex_imem_size);
        return 1;
    }
    fread(core->imem, 1, lSize, program_file);
    fclose(program_file);
    // Start the rVEX core
    return 0;
}

void start_rvex_programs(rvex device) {
    for (int stream = 0; stream < device.number_of_streams; ++stream) {
        for (int core = 0; core < device.number_of_cores; ++core) {
            // Start the rVEX core
            *device.streams[stream].cores[core].creg = byte_swap_w(0x80000000);

        }
    }
}

/*
 * Function that downloads precombiled binaries to the
 * rVEX cores.
 * There has to be a binary for each stage of the rVEX
 * pipline and all given the same name but with a number
 * at the end starting at zero for the first kernel.
 */
int create_program_with_binary(context *ctx, rvex *rvex, char program[]) {
    char kernel_name[32];
    for (int stream = 0; stream < rvex->number_of_streams; ++stream) {
        for (int core = 0; core < rvex->number_of_cores; ++core) {
            // Build string for rvex_binary name
            snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, core);
            if (download_binary_to_rvex(&rvex->streams[stream].cores[core], kernel_name, ctx->imem_size))
                return 1;
        }
    }
    return 0;
}

int get_rect_size(
        int width,
        int height,
        int pixel_size,
        int streams,
        int mem_size,
        int *return_width,
        int *return_height) {

    int heights[height + 1];
    heights[0] = -1;
    for (int rect_height = 1; rect_height < height; ++rect_height) {
        if ((height / streams) % rect_height == 0)
            heights[rect_height] = rect_height;
        else
            heights[rect_height] = -1;
    }
    int new_max = 0;
    int old_max = 0;
    for (int rect_width = 1; rect_width < width * pixel_size; ++rect_width) {
        if (width * pixel_size % rect_width == 0 && rect_width % 3 == 0 && rect_width % 4 == 0)
            for (int rect_height = 0; rect_height < height; ++rect_height) {
                if (heights[rect_height] > 2) {
                    int rect_size = rect_height * rect_width;
                    if (rect_size < mem_size) {
                        int perimeter = 2 * (rect_height - 2) + 2 * rect_width;
                        new_max = rect_size / perimeter;
                        if (new_max > old_max) {
                            old_max = new_max;
                            *return_width = rect_width;
                            *return_height = rect_height;
                        }
                    }
                }
            }
    }
//    printf("rect width %d, rect height %d \n", *return_width, *return_height);
    return old_max > 0 ? 0 : 1;
}

int
set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams,
                     int padding, int rect_height, int rect_width, int number_of_rects) {
    ptr->out_rect.height = rect_height;
    ptr->out_rect.width = rect_width;
    ptr->out_rect.padded_width = padding;
    ptr->image_data = data;
    ptr->size = width * height * pixel_size;
    ptr->pixel_size = pixel_size;
    ptr->width = width;
    ptr->height = height;
    ptr->stride = width * pixel_size;
    ptr->number_of_rects = number_of_rects;
    return 0;
}

int greyscale_image(image_info *grey_image, image_info input_image) {
    grey_image->height = input_image.height;
    grey_image->width = input_image.width;
    grey_image->stride = input_image.width;
    grey_image->pixel_size = 1;
    grey_image->size = input_image.width * input_image.height;
    grey_image->out_rect = input_image.out_rect;
    return 0;
}

int update_output_address(rvex *device, int address, image_info input_image, image_info output_image) {
    int lines_per_stream = input_image.height / device->number_of_streams;
    for (int stream = 0; stream < device->number_of_streams; ++stream) {
        device->streams[stream].parameters->out_address = byte_swap_w(
                address + stream * lines_per_stream * output_image.stride);
    }
    return 0;
}

int set_transfer_parameters(transfer *trans, unsigned char *data, int rect_height, int rect_width, int width, int data_size, int stride){
    trans->rect_height = rect_height;
    trans->rect_width = rect_width;
    trans->width = width;
    trans->data_size = data_size;
    trans->stride = stride;
}

int set_rvex_parameters(rvex *device, int address, image_info input_image, image_info output_image) {
    int lines_per_stream = input_image.height / device->number_of_streams;
    for (int stream = 0; stream < device->number_of_streams; ++stream) {
        device->streams[stream].parameters->state = IDLE;
        device->streams[stream].parameters->stream_number = byte_swap_w(stream);
        device->streams[stream].parameters->out_address = byte_swap_w(
                address + stream * lines_per_stream * output_image.stride);
        device->streams[stream].parameters->rect_height = byte_swap_w(input_image.out_rect.height);
        device->streams[stream].parameters->rect_width = byte_swap_w(input_image.out_rect.width);
        device->streams[stream].lines_per_stream = lines_per_stream;
        device->streams[stream].input_image = input_image;
        device->streams[stream].output_image = output_image;
        device->streams[stream].input_image.image_data += stream * lines_per_stream * input_image.stride;
        device->streams[stream].parameters->stride = byte_swap_w(input_image.stride);
        device->streams[stream].parameters->width = byte_swap_w(input_image.width);
    }
    return 0;
}

int write_lines_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {

        if (pthread_create(
                &threads[stream],
                NULL,
                write_lines,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int write_padded_lines_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        if (pthread_create(
                &threads[stream],
                NULL,
                write_padded_lines,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int write_passive_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {

        if (pthread_create(
                &threads[stream],
                NULL,
                write_image,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int write_padded_image_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {

        if (pthread_create(
                &threads[stream],
                NULL,
                write_padded_output,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int write_image_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {

        if (pthread_create(
                &threads[stream],
                NULL,
                write_output_rect,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int write_rect(void *dest, int frame_height, int rect_width, int rect_height, int rect_origin, void *ptr,
               rvex_stream *stream, int frame_width, int rect_origin_out) {
    int offset = 0;
//    printf("offset is %d \n", rect_origin_out);
    stream->parameters->offset = byte_swap_w(rect_origin_out);
    for (int height = 0; height < rect_height; ++height) {
        offset = rect_origin + height * frame_width;
        // Copy one line of the rect to rVEX
        memcpy(dest + rect_width * height, ptr + offset, (size_t) rect_width);
    }
    return 0;
}

void *write_lines(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int number_of_iter = stream->input_image.number_of_rects / stream->number_of_streams;
    int chunk = stream->input_image.out_rect.height * stream->input_image.out_rect.width;
//    printf("number of lines %d \n", number_of_lines);
    int number_of_transfers = stream->lines_per_stream;
    for (int i = 0; i < number_of_iter; ++i) {
        while (stream->parameters->state != IDLE) {
            usleep(1);
        }
        int offset = (chunk * stream->output_image.pixel_size * i);
        stream->parameters->offset = byte_swap_w(offset);
//        printf("damn it %d \n",chunk);
        memcpy(stream->parameters->data, stream->input_image.image_data + chunk * stream->input_image.pixel_size * i,
               (size_t) (chunk * stream->input_image.pixel_size));

        stream->parameters->state = READY;

    }
}

void *write_threaded_lines(void *arg) {
    thread_info *info = (thread_info *) arg;
    rvex_stream stream = info->stream;
    int i = info->iteration;
    int chunk = stream.input_image.out_rect.height * stream.input_image.out_rect.width;
    int padding_offset = stream.input_image.out_rect.padded_width;

    while (stream.parameters->state != IDLE) {
        usleep(1);
    }
    stream.parameters->state = BUSY;
    int offset = (chunk * stream.output_image.pixel_size * i);
    stream.parameters->offset = byte_swap_w(offset);
    memcpy(stream.parameters->data,
           stream.input_image.image_data - padding_offset + chunk * stream.input_image.pixel_size * i,
           (size_t) (chunk * stream.input_image.pixel_size) + padding_offset * 2);

    stream.parameters->state = READY;


}

void *write_padded_lines(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int number_of_iter = stream->input_image.number_of_rects / stream->number_of_streams;
    int chunk = stream->input_image.out_rect.height * stream->input_image.out_rect.width;
    int padding_offset = stream->input_image.out_rect.padded_width;
    for (int i = 0; i < number_of_iter; ++i) {
        while (stream->parameters->state != IDLE) {
            usleep(1);
        }
        int offset = (chunk * stream->output_image.pixel_size * i);
        stream->parameters->offset = byte_swap_w(offset);


        memcpy(stream->parameters->data,
               stream->input_image.image_data - padding_offset + chunk * stream->input_image.pixel_size * i,
               (size_t) (chunk * stream->input_image.pixel_size) + padding_offset * 2);

        stream->parameters->state = READY;

    }
}


void *write_fixed_lines(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int number_of_iter = stream->input_image.number_of_rects / stream->number_of_streams;
    int chunk = stream->input_image.out_rect.height * stream->input_image.out_rect.width;
//    printf("number of lines %d \n", number_of_lines);
    int number_of_transfers = stream->lines_per_stream;
    for (int i = 0; i < number_of_iter; ++i) {
        while (stream->parameters->state != IDLE) {
            usleep(1);
        }
        int offset = (chunk * stream->output_image.pixel_size * i);
        stream->parameters->offset = byte_swap_w(offset);
        memcpy(stream->parameters->data, stream->input_image.image_data + chunk * stream->input_image.pixel_size * i,
               (size_t) (chunk * stream->input_image.pixel_size));
        stream->parameters->state = READY;
    }
    while (stream->parameters->state != IDLE) {
        usleep(1);
    }
}

void *write_image(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int number_of_iter = 50 / stream->number_of_streams;
    for (int i = 0; i < number_of_iter; ++i) {
        memcpy(stream->parameters->data, stream->input_image.image_data, 28800);
        stream->parameters->state = READY;
    }
}

void *write_output_rect(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int rect_width = stream->input_image.out_rect.stride;
    int rect_height = stream->input_image.out_rect.height;
    int number_of_lines = stream->lines_per_stream;
    int line_width = stream->input_image.stride;
    for (int y = 0; y < number_of_lines; y += rect_height) {
        for (int x = 0; x < line_width; x += rect_width) {
            int origin = y * line_width + x;
            while (stream->parameters->state != IDLE) {
                usleep(1);
            }
            write_rect(stream->parameters->data, 0, rect_width, rect_height, origin, stream->input_image.image_data,
                       stream,
                       line_width, origin);
            stream->parameters->state = READY;

        }
    }
    while (stream->parameters->state != IDLE) {
        usleep(1);
    }
}

void *write_padded_output(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int rect_width = stream->input_image.out_rect.width;
    int rect_height = stream->input_image.out_rect.height;
    for (int y = 0; y < stream->lines_per_stream; y += rect_height) {
        for (int x = 0; x < stream->input_image.width; x += rect_width) {
            int origin = y * stream->input_image.stride + x * stream->input_image.pixel_size;
            int origin_out = y * stream->output_image.stride + x * stream->output_image.pixel_size;
            while (stream->parameters->state != IDLE) {
                usleep(1);
            }
            write_rect(stream->parameters->data,
                       0,
                       stream->input_image.out_rect.padded_width,
                       stream->input_image.out_rect.padded_height,
                       origin,
                       stream->input_image.image_data,
                       stream,
                       stream->input_image.stride,
                       origin_out);
            stream->parameters->state = READY;
        }
    }
}

void *write_and_pad_output_rect(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int rect_width_padded = stream->input_image.out_rect.stride + 2 * stream->input_image.pixel_size;
    int rect_height_padded = stream->input_image.out_rect.height + 2;
    int number_of_lines = stream->lines_per_stream;
    int line_width = stream->input_image.stride;
    unsigned char *pad;

    if (stream->stream_number == 0 || stream->stream_number == stream->number_of_streams - 1)
        pad = calloc((size_t) (rect_width_padded + 3), sizeof(unsigned char));
    for (int y = 0; y < number_of_lines; y += stream->input_image.out_rect.height) {
        for (int x = 0; x < line_width; x += stream->input_image.out_rect.stride) {
            int origin = y * line_width + x;
            while (stream->parameters->state != IDLE) {
                usleep(1);
            }
            stream->parameters->state = BUSY;
            // if y == 0 and first stream take care of extra padding
            if (stream->stream_number == 0 && y == 0) {
                // if x is 0 there should be an extra trick
                if (x == 0) {
                    memcpy(stream->parameters->data, pad, (size_t) rect_width_padded + 3);
                    memcpy(stream->parameters->data + rect_width_padded + 3, stream->input_image.image_data,
                           (size_t) rect_width_padded - 3);
                } else {
                    memcpy(stream->parameters->data, pad, (size_t) rect_width_padded);
                    memcpy(stream->parameters->data + rect_width_padded, stream->input_image.image_data + x - 3,
                           (size_t) rect_width_padded);
                }
                write_rect(stream->parameters->data + 2 * rect_width_padded, 0, rect_width_padded,
                           rect_height_padded - 2, origin, stream->input_image.image_data + line_width, stream,
                           line_width, 0);

                // check for the last stream as its last line is special
            } else if (stream->stream_number == stream->number_of_streams - 1 &&
                       number_of_lines - y == stream->input_image.out_rect.height) {
                // write all but last line
                write_rect(stream->parameters->data, 0, rect_width_padded, rect_height_padded - 2, origin,
                           stream->input_image.image_data, stream, line_width, 0);

                int last_offset = (rect_height_padded - 2) * rect_width_padded;
                if (x == line_width - stream->input_image.out_rect.stride) {
                    // last rect
                    memcpy(stream->parameters->data + last_offset,
                           stream->input_image.image_data + origin - 3 + (rect_height_padded - 3) * line_width,
                           (size_t) rect_width_padded - 3);
                    memcpy(stream->parameters->data + last_offset + rect_width_padded - 3, pad,
                           (size_t) rect_width_padded + 3);
                } else {
                    // pad bottom line
                    memcpy(stream->parameters->data + last_offset,
                           stream->input_image.image_data + origin - 3 + (rect_height_padded - 3) * line_width,
                           (size_t) rect_width_padded);
                    memcpy(stream->parameters->data + last_offset + rect_width_padded, pad, (size_t) rect_width_padded);
                }


            } else {

                write_rect(stream->parameters->data, 0, rect_width_padded, rect_height_padded, origin,
                           stream->input_image.image_data, stream, line_width, 0);

            }
            stream->parameters->state = READY;

        }
    }
}

void free_context(context *context) {
    //TODO update me please
//    munmap((void *) context->input_frame_buffer, (size_t) context->frame_size);
    close(context->memory_handler);
}


int write_image_to_file(context ctx, int address, long size, const char *file) {
    // Reconsturct image_data
    unsigned char *out_image = (unsigned char *) mmap(
            NULL,
            (size_t) size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            address);

    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".out");

    // Open device configuration to download bitstream
    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
        putc(out_image[i], out);
    }
    fclose(out);
    return 0;
}

int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size) {
    // load dimensions
    char dim_file[128];
    strcpy(dim_file, file);
    strcat(dim_file, ".dim");
    FILE *df;
    char *line = NULL;
    size_t len = 0;
    df = fopen(dim_file, "r");
    if (df == NULL) {
        printf("failed to open .dim file \n");
        return 1;
    }
    getline(&line, &len, df);
    (*width) = atoi(line);
    getline(&line, &len, df);
    (*height) = atoi(line);
    getline(&line, &len, df);
    (*pixel_size) = atoi(line);
//    printf("len %d \n", (int) len);
    fclose(df);
    free(line);
    return 0;
}

int get_image_from_file(const char *input_file, unsigned char *image_data, long size) {
    // load image
    FILE *blob = fopen(input_file, "r");
    if (!blob) {
        printf("failed to open image file %s\n", input_file);
        return 1;
    }

    fread(image_data, 1, (size_t) size, blob);
    fclose(blob);

    return 0;
}
