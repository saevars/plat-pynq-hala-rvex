//
// Created by saevar on 12/29/17.
//

#ifndef SRC_PALLOC_H
#define SRC_PALLOC_H

int get_memory_address(long size, int *pInt);
/*
 * Free all memory allocated by the server
 */
int free_buffers();

/*
 * Get an active input address that the VDMA is writing to.
 * Takes a pointer as a parameter, returns 0 if successful
 */
int get_input_address_from_VDMA( int *address);

/*
 * Gets an active output address that the VDMA is reading from.
 * Takes a pointer as a parameter, returns 0 if successful
 */
int get_output_address_from_VDMA(int *address);

/*
 * Stops the HDMI's input and output channels.
 */
int stop_VDMA();

/*
 * Starts both HDMI in and HDMI out,
 *
 */
int start_VDMA();

#endif //SRC_PALLOC_H
