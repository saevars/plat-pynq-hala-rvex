//
// Created by saevar on 2/16/18.
//

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <time.h>
#include "palloc.h"
#include "hala_rvex.h"

int main(){
    int number_of_streams = 1;
    int number_of_cores = 1;

    int error;
    context ctx;
    error = initialise_context(&ctx);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }

    char *bitstream = NULL;
    size_t bitstream_size;


    printf("Reading bitstream from file \n");
    error = read_bitstream_from_file(&ctx,
                                     number_of_streams,
                                     number_of_cores,
                                     &bitstream,
                                     &bitstream_size);
    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }

    error = download_bitstream(bitstream, bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }
    free(bitstream);

    // Start the VDMA server
    printf("Starting HDMI input and output \n");
    start_VDMA();

    rvex device;
    error = initialise_rvex(&ctx, &device, number_of_streams, number_of_cores);
    if (error) {
        printf("failed to initialise rvex \n");
        return 1;
    }

    error = create_program_with_binary(&ctx, &device, "gaussian-sobellines");
    if (error) {
        perror("failed to download kernel \n");
        return 1;
    }

    // Get input frame address from VDMA server
    int input_address;
    get_input_address_from_VDMA(&input_address);
    printf("input address %x \n", input_address);

    // get output address
    int output_address;
    get_output_address_from_VDMA(&output_address);
    printf("output address: %x \n", output_address);

    unsigned char *in_image_data  = (unsigned char *) mmap(
            NULL,
            (size_t) 800*600*3 + 2*4096,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            input_address - 4096);
    in_image_data += 4096;
    image_info input_image, output_image;
    error = set_image_properties(&input_image, in_image_data, 800, 600, 1, number_of_streams, 0, 0, 0, 0);
    if (error) {
        printf("failed to set image properties \n");
        return (1);
    }
    input_image.number_of_rects = 40;
    input_image.out_rect.width = 800;
    input_image.out_rect.height = 15;
    input_image.out_rect.padded_width = 1601;
    error = greyscale_image(&output_image, input_image);
    if (error) {
        printf("failed to set greyscale image \n");
        return (1);
    }
    printf("setting up parmeters\n");
    // Setup default transfer parameters
    error = set_rvex_parameters(&device, output_address, input_image, output_image);
    if (error) {
        perror("failed to set rVEX parameters \n");
        return 1;
    }

    printf("starting up rVEX cores\n");
    start_rvex_programs(device);

    int iterations = 100;
    printf("writing image to rvex \n");
    printf("number of iterations: %d \n", iterations);
    int start_frame_seconds = (int) time(NULL);
    for (int j = 0; j < iterations; ++j) {
        error = write_padded_lines_to_rvex(&device, input_image);
        if (error) {
            perror("failed to write image ro rVEX \n");
            return 1;
        }
    }
    int stop_frame_seconds = (int) time(NULL);
    double delta = stop_frame_seconds - start_frame_seconds;
    double fps = iterations / delta;
    printf("time per image %f \n", delta / iterations);
    printf("fps: %f \n", fps);
    //Stop the VDMA channels
    stop_VDMA();
    return 0;
}