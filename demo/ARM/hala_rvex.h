//
// Created by saevar on 11/27/17.
//

#ifndef SRC_HALA_RVEX_H
#define SRC_HALA_RVEX_H

#include "common.h"

#define RVEX "rvex"

typedef struct {
    int x;
    int y;
    int stride;
    int height;
    int width;
    int padded_width;
    int padded_height;
} rectangle;

typedef struct {
    int height;
    int width;
    int pixel_size;
    int stride;
    long size;
    int number_of_rects;
    rectangle out_rect;
    unsigned char *image_data;
} image_info;

typedef struct {
    off_t imem_address;
    off_t creg_address;
    char *imem;
    int *creg;
} rvex_core;

typedef struct {
    unsigned char *dmem;
    transfer *base;
    int *data_offset;
    int stream_number;
    unsigned char *busy;
    unsigned char *input_framebuffer;
    int bytes_per_stream;
    int device_offset;
    int lines_per_stream;
    int number_of_streams;
    int stride;
    int *out_address;
    rvex_core *cores;
    transfer *parameters;
    image_info input_image;
    image_info output_image;
} PACK rvex_stream;

typedef struct {
    rvex_stream stream;
    int iteration;
} thread_info;


typedef struct {
    int rvex_handler;
    rvex_stream *streams;
    long base_address;
    int frame_address;
    int pixel_length;
    int fb_length;
    unsigned char *input_framebuffer;
    unsigned char *output_framebuffer;
    unsigned char *rvex_inputmem;
    unsigned char *rvex_outputmem;
    unsigned char *input_address;
    unsigned char *output_address;
    int number_of_streams;
    int number_of_cores;
    unsigned int *vdma;
} rvex;

typedef struct {
    rvex *device;
    int number_of_streams;
    int number_of_cores;
    int core_offset;
    int stream_offset;
    long rvex_base_address;
    int dmem_size;
    int imem_size;
    int dmem_offset;
    int imem_offset;
    int creg_offset;
    int memory_handler;
} context;

typedef struct {
    rvex_stream *stream;
    int offset;
} thread_stream;

int initialise_context(context *context);

int read_bitstream_from_file(context *ctx, int number_of_streams, int number_of_cores, char **buffer, size_t *size);

/*
 * Function that take bitstream as a parameter and downloads
 * it to the PL via /deve/xdevcfg
 * Care has to be taken to shutdown all data-
 * transfers before hand, like shutting down the VDMA
 * Needs root privileges
 */
int download_bitstream(char *buffer, size_t size);

int download_binary_to_rvex(rvex_core *core, char *program, int rvex_imem_size);

int create_program_with_binary(context *ctx, rvex *rvex, char program[]);

int initialise_rvex(context *context, rvex *rvex, int number_of_streams, int number_of_cores);

void *write_output(void *arg);

int write_out(rvex_stream *stream);

void free_context(context *context);

void *write_output_rect(void *arg);

void *write_padded_lines(void *arg);

void *write_lines(void *arg);

int write_padded_lines_to_rvex(rvex *device, image_info image);

int write_super_threaded_to_rvex(rvex *device, image_info image);

void *write_and_pad_output_rect(void *arg);

void *write_threaded_lines(void *arg);

int set_rvex_parameters(rvex *device, int address, image_info input_image, image_info output_image);

int write_image_to_rvex(rvex *device, image_info image);

int write_passive_to_rvex(rvex *device, image_info image);

void *write_image(void *arg);

int
set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams,
                     int padding, int rect_height, int rect_width, int number_of_rects);

void *write_padded_output(void *arg);

void start_rvex_programs(rvex device);

int greyscale_image(image_info *grey_image, image_info input_image);

int write_padded_image_to_rvex(rvex *device, image_info image);

int write_lines_to_rvex(rvex *device, image_info image);

int update_output_address(rvex *device, int address, image_info input_image, image_info output_image);

int write_image_to_file(context ctx, int address, long size, const char *file);

int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size);

/*
 * Reads data from file input_filse into data array image_data of size size
 */
int get_image_from_file(const char *input_file, unsigned char *image_data, long size);

int get_rect_size(
        int width,
        int height,
        int pixel_size,
        int streams,
        int mem_size,
        int *return_width,
        int *return_height);


#endif //SRC_HALA_RVEX_H
