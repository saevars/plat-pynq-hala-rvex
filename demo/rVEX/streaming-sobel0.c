// Created by saevar on 2/24/18.
//

#include "common.h"

int main() {
    transfer *in = INPUT_MEM;
    volatile char *state;
    state = &in->state;
    transfer *out = (transfer *) OUTPUT_MEM;
    volatile char *out_state;
    out_state = &out->state;
    int image_width = in->width;
    int last_pixel = (in->rect_height + 3) * in->rect_width + 2;
    out->width = in->width;
    out->rect_height = in->rect_height;
    out->rect_width = in->rect_width;
#pragma unroll(0)
    while (1) {
        if (*state == READY) {
            in->state = BUSY;
            out->out_address = in->out_address;
#pragma unroll(8)
            for (int pixel = image_width; pixel < 3203; pixel++) {
                int offset_pixel = pixel;
                int p = in->data[offset_pixel - image_width - 1];
                p += in->data[offset_pixel - image_width] * 2;
                p += in->data[offset_pixel - image_width + 1];
                p += in->data[offset_pixel - 1] * 2;
                p += in->data[offset_pixel] * 4;
                p += in->data[offset_pixel + 1] * 2;
                p += in->data[offset_pixel + image_width - 1];
                p += in->data[offset_pixel + image_width] * 2;
                p += in->data[offset_pixel + image_width + 1];
                out->data[pixel - image_width] = (unsigned char) (p / 16);
            }
//            while (*out_state != IDLE) {}

            out->offset = in->offset;
            out->state = READY;


#pragma unroll(8)
            for (int pixel = 3203; pixel < last_pixel; pixel++) {
                int offset_pixel = pixel;
                int p = in->data[offset_pixel - image_width - 1];
                p += in->data[offset_pixel - image_width] * 2;
                p += in->data[offset_pixel - image_width + 1];
                p += in->data[offset_pixel - 1] * 2;
                p += in->data[offset_pixel] * 4;
                p += in->data[offset_pixel + 1] * 2;
                p += in->data[offset_pixel + image_width - 1];
                p += in->data[offset_pixel + image_width] * 2;
                p += in->data[offset_pixel + image_width + 1];
                out->data[pixel - image_width] = (unsigned char) (p / 16);

            }
            in->state = IDLE;


        }
    }
}
