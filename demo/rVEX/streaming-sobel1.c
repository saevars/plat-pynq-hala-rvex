
//
// Created by saevar
// on 2/24/18.
//
#include "common.h"

int main() {
    transfer *in = INPUT_MEM;
    int image_width = 800;//in->width;
    int out_size = (in->rect_width / 4) - 1;
    volatile char * state;
    state = &in->state;
    *state = IDLE;
#pragma unroll(0)
    while (1) {
        if ( *state == READY) {
            int offset = in->offset;
            in->state = BUSY;
            for (int number_of_iterations = 0; number_of_iterations < 30; ++number_of_iterations) {
                int int_counter = 3;
                unsigned char tmp[sizeof(int)];
                volatile int *buf = *(int *volatile *) OUTPUT_MEM;
                int addr = in->out_address + offset + number_of_iterations * 800;
                *buf++ = addr;
                int offsets = 800* number_of_iterations;
#pragma unroll(8)
                for (int pixel = 0; pixel < 800; pixel++) {
                    int offset_pixel = pixel + image_width+1 + offsets;
                    int px = in->data[offset_pixel - image_width - 1];
                    px -= in->data[offset_pixel - image_width + 1];
                    px += in->data[offset_pixel - 1] * 2;
                    px -= in->data[offset_pixel + 1] * 2;
                    px += in->data[offset_pixel + image_width - 1];
                    px -= in->data[offset_pixel + image_width + 1];


                    int py = in->data[offset_pixel - image_width - 1];
                    py += in->data[offset_pixel - image_width] * 2;
                    py += in->data[offset_pixel - image_width + 1];
                    py -= in->data[offset_pixel + image_width -1];
                    py -= in->data[offset_pixel + image_width] * 2;
                    py -= in->data[offset_pixel + image_width + 1];
                    int sobel = (px < 0 ? -px : px) + (py < 0 ? -py : py);
                    tmp[int_counter] = (unsigned char) (sobel > 255 ? 255 : sobel);
                    int_counter--;
                    if (int_counter == -1) {
                        buf[(pixel) >> 2] = *((int *) tmp);
                        int_counter = 3;
                    }
                }
                buf = (int *) ((int) buf & ~0x7F);
//#pragma unroll(0)
//                while (*((volatile int *) OUTPUT_MEM) & (1 << 12)) {};

                // Start the transfer.
                *buf = out_size;
            }

            in->state = IDLE;

        }
    }

// Crash
    while (1) {
    }

    return 0;
}

