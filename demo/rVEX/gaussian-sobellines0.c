//
// Created by saevar on 2/24/18.
//
#include "common.h"

int main() {
    transfer *in = INPUT_MEM;
    volatile char *state;
    state = &in->state;
    int image_width = 800;
    int last_pixel = 15*800+2402;
    // tux
//    int image_width = 256;
//    int last_pixel = 8 * 1024 + 770;
#pragma unroll(0)
    while (1) {
        if (*state == READY) {
            in->state = BUSY;
            unsigned char gaussian[15 * 800 + 1602];
            // tux
//            unsigned char gaussian[8 * 1024 + 514];
            int offset = in->offset;
#pragma unroll(4)
            for (int pixel = image_width; pixel < last_pixel; pixel++) {
                int offset_pixel = pixel;
                int p = in->data[offset_pixel - image_width - 1];
                p += in->data[offset_pixel - image_width] * 2;
                p += in->data[offset_pixel - image_width + 1];
                p += in->data[offset_pixel - 1] * 2;
                p += in->data[offset_pixel] * 4;
                p += in->data[offset_pixel + 1] * 2;
                p += in->data[offset_pixel + image_width - 1];
                p += in->data[offset_pixel + image_width] * 2;
                p += in->data[offset_pixel + image_width + 1];
                gaussian[pixel - image_width] = (unsigned char) (p / 16);
            }

            in->state = IDLE;

            int out_size = 199;
            // tux
//            int out_size = 255;
            for (int number_of_iterations = 0; number_of_iterations < 15; ++number_of_iterations) {
                int int_counter = 3;
                unsigned char tmp[sizeof(int)];
                volatile int *buf = *(int *volatile *) OUTPUT_MEM;
                int addr = in->out_address + offset + number_of_iterations * 800;
                *buf++ = addr;
                int offsets = 800 * number_of_iterations;
#pragma unroll(8)
                for (int pixel = 0; pixel < 800; pixel++) {
                    int offset_pixel = pixel + image_width + 1 + offsets;
                    int px = gaussian[offset_pixel - image_width - 1];
                    px -= gaussian[offset_pixel - image_width + 1];
                    px += gaussian[offset_pixel - 1] * 2;
                    px -= gaussian[offset_pixel + 1] * 2;
                    px += gaussian[offset_pixel + image_width - 1];
                    px -= gaussian[offset_pixel + image_width + 1];


                    int py = gaussian[offset_pixel - image_width - 1];
                    py += gaussian[offset_pixel - image_width] * 2;
                    py += gaussian[offset_pixel - image_width + 1];
                    py -= gaussian[offset_pixel + image_width - 1];
                    py -= gaussian[offset_pixel + image_width] * 2;
                    py -= gaussian[offset_pixel + image_width + 1];
                    int sobel = (px < 0 ? -px : px) + (py < 0 ? -py : py);
//                    sobel = sobel < 40 ? 0 : sobel;
                    tmp[int_counter] = (unsigned char) (sobel > 255 ? 255 : sobel);
                    int_counter--;
                    if (int_counter == -1) {
                        buf[(pixel) >> 2] = *((int *) tmp);
                        int_counter = 3;
                    }
                }
                buf = (int *) ((int) buf & ~0x7F);
#pragma unroll(0)
                while (*((volatile int *) OUTPUT_MEM) & (1 << 12)) {};

// Start the transfer.
                *buf = out_size;
            }

        }
    }
}