//
// Created by saevar on 10/31/17.
//

// Ensure identical packing by all compilers. This *should* not be necessary
// as long as all compilers are satisfied with 32-bit pos/size alignment of
// structs, and n-bit alignment of n-bit ints.
//#define PACK __attribute__((_packed_))
#define PACK

#ifndef SRC_COMMON_H
#define SRC_COMMON_H


#define NUM_CORES   1
#define NUMBER_OF_STREAMS 5
#define INPUT_MEM               0x00000000
#define OUTPUT_MEM              0x80000000
#define FRAME_ADDRESS_INPUT     0x17000000
#define FRAME_ADDRESS_OUTPUT    0x16900000
#define VDMA_ADDRESS            0x43000000
#define RVEX_BASE_ADDRESS       0x80000000

/* Register offsets */
#define OFFSET_BUSY             0x01
#define OFFSET_CHUNK_COUNTER    0x04
#define OFFSET_OUT_ADDRESS      0x08
#define OFFSET_INPUT_DATA       0x0C
#define OFFSET_RVEX_READY                       0xCD0
#define OFFSET_RVEX_OUTMEM                      0x30000
#define RVEX_DMEM_SIZE                          0x8000
#define RVEX_IMEM_SIZE                          0x1000
#define RVEX_FLAG_SIZE                          0x4

#define WIDTH 256
#define HEIGHT 256
#define STRIDE 3
#define CHUNK 960
#define CHUNK_GRAY CHUNK/3
#define BIT_PER_PIXEL 8
#define RECT_HEIGHT 32
#define RECT_WIDTH 64
#define COLOR_PIXEL 3
#define PIXELS_PER_PADDED_RECT 10

#define READY 1
#define BUSY -1
#define IDLE 0

typedef struct {
    unsigned char r;
    unsigned char g;
    unsigned char b;
}PACK pixel;

typedef struct {
    int stream_number;
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    volatile int rect_height;
    unsigned char data[];
} PACK transfer;

typedef struct {
    int stream_number;
    char busy;
    void *data;
} rvex_parameters;

// rect height is 40 + 2 and width is 24 + 2*3
typedef struct {
    volatile unsigned char data[RECT_HEIGHT + 2][RECT_WIDTH + 2 * COLOR_PIXEL];
    volatile int state;
    volatile int offset;
}PACK input_rect;

typedef struct {
    volatile unsigned char data[RECT_HEIGHT][RECT_WIDTH];
    volatile int state;
    volatile int offset;
}PACK inputs;

// rect height is 40 + 2 and width is 24 + 2*3
typedef struct {
    volatile int state;
    volatile int offset;
    volatile int out_address;
    volatile unsigned char data[RECT_HEIGHT + 2][(RECT_WIDTH + 2 * COLOR_PIXEL) / 3];
}PACK grey_rect;


typedef struct {
    volatile pixel data[42][PIXELS_PER_PADDED_RECT];
    volatile int state;
    volatile int offset;
}PACK rect_data0;

typedef struct {
    //
    volatile pixel data[42][PIXELS_PER_PADDED_RECT];
    volatile int state;
    volatile int offset;
    volatile int state_core_1;
}PACK rect_data01;

typedef struct {
    volatile pixel data[42][PIXELS_PER_PADDED_RECT];
    volatile int state;
    volatile int offset;
}PACK rect_data12;

typedef struct {
    volatile unsigned char data[40][24];
    volatile int state;
    volatile int offset;
}PACK rect_data;
// Data shared between core 0 and 1.
typedef struct {

    // State:
    //  - written to 0 by c1 initialization.
    //  - written to -1 by c0 after updating line width/mandel/filter
    //    parameters. c1 should then propagate the new parameters to the next
    //    core.
    //  - written to x by c0 when line[2-x] contains data that c1 needs to
    //    process further.
    //     - 1: c0 = line[0], c1 = line[1]
    //     - 2: c0 = line[1], c1 = line[0]
    //     - *: c0 = line[0,1], c1 = -
    //  - written to 0 by c1 when it finishes its job.
    volatile int state;

    // Output line width (excluding extra data needed for the filters).
    volatile int line_width;

    // Framebuffer stride.
    volatile int stride;

    volatile int chunk_number;

    volatile unsigned char data[CHUNK];

} PACK c0_c1_t;

// Data shared between core 1 and 2.
typedef struct {

    // State:
    //  - written to 0 by c2 initialization.
    //  - written to -1 by c1 after updating line width/filter parameters. c2
    //    should then propagate the new parameters to the next core.
    //  - written to x by c1 when the five lines following x-1 (modulo 6)
    //    contain valid data.
    //     - 1: c1 = line[0], c2 = line[1,2,3,4,5]
    //     - 2: c1 = line[1], c2 = line[2,3,4,5,0]
    //     - 3: c1 = line[2], c2 = line[3,4,5,0,1]
    //     - 4: c1 = line[3], c2 = line[4,5,0,1,2]
    //     - 5: c1 = line[4], c2 = line[5,0,1,2,3]
    //     - 6: c1 = line[5], c2 = line[0,1,2,3,4]
    //     - *: c1 = line[0,1,2,3,4,5], c2 = -
    //  - written to 0 by c2 when it finishes its job.
    volatile int state;

    // Output line width (excluding extra data needed for the filters).
    volatile int line_width;

    // Framebuffer stride.
    volatile int stride;

    volatile int chunk_number;

    volatile unsigned char data[CHUNK];

    // Filter parameters (core 2).
//    volatile filter_param_t core2_filter_params;

    // Filter parameters (core 3).
//    volatile filter_param_t core3_filter_params;

    // Line buffer from c1 to c2. If state is positive, all buffers except
    // state-1 may not be modified by c1. Otherwise, c1 has access to all
    // buffers.
//    volatile linebuf_t line[6];

} PACK c1_c2_t;

// Data shared between the microblaze and core 0.
typedef struct {

    // State:
    //  - written to 0 by c0 initialization.
    //  - written to 1 by mb after writing all parameters below; waited on by
    //    c0.
    //  - written to 0 when c0 detects lines_done >= line_count; waited on by
    //    mb before writing new command parameters.
    volatile int state;

    // Number of lines completed.
    //  - written to -C0_EXTRA_PIXELS by mb before the job starts.
    //  - incremented by c0 when advancing to the next line.
    volatile int lines_done;

    // Number of lines to generate.
    volatile int line_count;

    // Output line width (including extra data needed for the filters).
    volatile int line_width;

    // Framebuffer stride.
    volatile int stride;

    // Top-left output pixel coordinates.
    volatile int x;
    volatile int y;


    // This is a lookup table for use by the mandelbrot generator. It must be
    // initialized to the following by mb:
    // [int(round((1 - math.log(math.log(math.sqrt((i+16)/4.)))/math.log(2))*16)) for i in range(128)]
    // It is used to smooth the coloring between the iteration counts.
    unsigned char smooth_color[128];

    // This is a lookup table for use by the mandelbrot generator. It must be
    // initialized to the following by mb:
    // a = [int(round(((1+math.cos(min(i/220., 1.)*math.pi*2.))/2.)*255)) for i in range(256)]
    // [a[i] + a[(i+84)%256]*256 + a[(i+168)%256]*65536 for i in range(256)]
    // It is the color lookup table for the mandelbrot.
    int rainbow[256];

    // This is a lookup table for use by the mandelbrot generator. It is a 1
    // bpp bitmap that maps to -1.5 to 0.5 re, 1 to 0 im. When a bit is set, it
    // is assumed that all values within that square are in the mandelbrot set.
    // One bit is 1/32 wide and high.
    int mandel_known[64];

    volatile unsigned char data[CHUNK];

} PACK arm_c0_t;


// Data shared between core 2 and 3.
typedef struct {

    // State:
    //  - written to 0 by c3 initialization.
    //  - written to -1 by c2 after updating line width/filter parameters. c3
    //    should then propagate the new parameters to the next core.
    //  - written to x by c2 when the five lines following x-1 (modulo 6)
    //    contain valid data.
    //     - 1: c2 = line[0], c3 = line[1,2,3,4,5]
    //     - 2: c2 = line[1], c3 = line[2,3,4,5,0]
    //     - 3: c2 = line[2], c3 = line[3,4,5,0,1]
    //     - 4: c2 = line[3], c3 = line[4,5,0,1,2]
    //     - 5: c2 = line[4], c3 = line[5,0,1,2,3]
    //     - 6: c2 = line[5], c3 = line[0,1,2,3,4]
    //     - *: c2 = line[0,1,2,3,4,5], c3 = -
    //  - written to 0 by c3 when it finishes its job.
    volatile int state;

    // Output line width (excluding extra data needed for the filters).
    volatile int line_width;

    // Framebuffer stride.
    volatile int stride;

    // Framebuffer stride.
    volatile int chunk_number;
    // If nonzero, the leftmost pixel of each line is set to this color.
    volatile int marker;

    volatile unsigned char data[CHUNK];

} PACK c2_c3_t;


typedef struct {
    arm_c0_t *data;
    unsigned int dmem;
    unsigned int imem;
    unsigned int creg;
} core;

//typedef struct {
//    core cores[NUM_CORES];
//    unsigned char *dmem;
//    unsigned char *base;
//    int *chunk_counter;
//    unsigned char *stream_number;
//    volatile unsigned char *busy;
//
//    unsigned char *infb;
//    int bytes_per_stream;
//    int number_of_streams;
//} rvex_stream;

typedef struct {
    unsigned int baseAddr;
    int rvexHandler;
//    rvex_stream *streams;
    int baseAddress;
    int frameAddress;
    int pixelLength;
    int fbLength;
    unsigned char *inputFrameBuffer;
    unsigned char *outputFrameBuffer;
    unsigned char *rvexInputMem;
    unsigned char *rvexOutputMem;
    unsigned char *inputAddress;
    unsigned char *outputAddress;
    int number_of_streams;
    unsigned int *vdma;
} rvex_handle;

typedef struct data_unit {
    unsigned char c0;
    unsigned char c1;
    unsigned char c2;
    unsigned char c3;
} data_unit;

#endif //SRC_COMMON_H