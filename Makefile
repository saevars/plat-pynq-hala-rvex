
.NOTPARALLEL:
.SUFFIXES:

.PHONY: all
all:
	$(MAKE) toolchain

#------------------------------------------------------------------------------
# Recursion into the toolchain directory
#------------------------------------------------------------------------------

TOOLCHAIN = toolchain/build-main
TOOLCHAIN_DEPS = main-config.ini

.PHONY: toolchain
toolchain: $(TOOLCHAIN)

$(TOOLCHAIN): $(TOOLCHAIN_DEPS)
	$(MAKE) -C toolchain


